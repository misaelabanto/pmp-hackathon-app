import 'package:flutter/services.dart';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String> openBotonPago(List<Producto> productos) async {
  const platform = const MethodChannel('fiis.uni.edu.pe/hackathon_pmp');
  String response;
  String nombres;
  String email;
  String apellidos;
  SharedPreferences.getInstance().then((sp) async {
    nombres = sp.getString("nombres");
    email = sp.getString("apellidos");
    apellidos = sp.getString("email");
    try {
      final String result = await platform.invokeMethod('showBotonPago', {
        "email": email,
        "nombres": nombres,
        "apellidos": apellidos
      });
      response = result;
    } on PlatformException catch (e) {
      print(e.toString());
      response = "No se pudo completar la compra";
    }
  });
  return response;
}