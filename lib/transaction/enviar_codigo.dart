import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> enviarCodigo ({String telefono, String idCliente}) async{
  print('ENVIAR CODIGO: ');
  print('TELEFONO: ' + telefono);
  print('ID_CLIENTE: ' + idCliente);
  http.Response response = await http.post('https://pmp-hackathon.herokuapp.com/api/sms/send',
    body: '{'
      '"number": "51$telefono",'
      '"idCliente": "$idCliente"'
      '}',
    encoding: Encoding.getByName('utf-8'),
    headers: {
      'Content-Type': 'application/json'
    });

  print(response.body);
  return jsonDecode(response.body);
}