import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> verificarCodigo ({String telefono, String idCliente, String codigo}) async{
  http.Response response = await http.post('https://pmp-hackathon.herokuapp.com/api/sms/verify',
    body: jsonEncode({
      'number': '51$telefono',
      'idCliente': idCliente,
      'code': codigo
    }),
    encoding: Encoding.getByName('utf-8'),
    headers: {
      'Content-Type': 'application/json'
    });

  print(response.body);

  return jsonDecode(response.body);
}