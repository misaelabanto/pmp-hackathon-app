import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:hackathon_pmp/model/cliente.dart';
import 'package:hackathon_pmp/model/producto.dart';

Future<String> registrarCompra ({String idCliente, List<Producto> productos}) async {
  http.Response response = await http.post('https://pmp-hackathon.herokuapp.com/api/venta/registrar',
    body: jsonEncode({
      'idCliente': idCliente,
      'productos': jsonEncode(productos)
    }),
    encoding: Encoding.getByName('utf-8'),
    headers: {
      'Content-Type': 'application/json'
    });

  var json = jsonDecode(response.body);
  print(json);
  return json;
}