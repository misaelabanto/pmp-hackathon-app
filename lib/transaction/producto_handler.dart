import 'dart:async';
import 'dart:convert';
import 'package:hackathon_pmp/model/cliente.dart';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:http/http.dart' as http;

class ProductoHandler {
  static Future<List<Producto>> getProductos () async{
    http.Response response = await http.get('https://pmp-hackathon.herokuapp.com/api/producto/listar',
      headers: {
        'Content-Type': 'application/json'
      });

    List<dynamic> result = jsonDecode(response.body);
    List<Producto> productos = [];
    for(dynamic map in result) {
      productos.add(Producto.fromJson(map));
    }
    return productos;
  }

  static Future<List<Producto>> getProductosPorCategoria() async {

  }
}
