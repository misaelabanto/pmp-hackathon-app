import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:hackathon_pmp/model/cliente.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Cliente> iniciarSesion ({String email, String password}) async{
  print(email + password);
  http.Response response = await http.post('https://pmp-hackathon.herokuapp.com/api/cliente/auth',
    body: jsonEncode({
      'email': email,
      'password': password
    }),
    encoding: Encoding.getByName('utf-8'),
    headers: {
      'Content-Type': 'application/json'
    });

  var json = jsonDecode(response.body);
  print(json);
  if(json['error']) return null;
  Cliente cliente = Cliente.fromJson(json);
  SharedPreferences.getInstance().then((sp) {
    sp.setString("nombres", cliente.nombres);
    sp.setString("apellidos", cliente.apellidos);
    sp.setString("email", cliente.email);
  });
  return cliente;
}