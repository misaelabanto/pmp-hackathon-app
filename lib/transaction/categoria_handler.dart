import 'dart:async';
import 'dart:convert';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:http/http.dart' as http;

class CategoriaHandler {
  static Future<List<dynamic>> getCategiorias() async {
    http.Response response = await http.get(
      'https://pmp-hackathon.herokuapp.com/api/categoria/listar',
      headers: {
        'Content-Type': 'application/json'
      });

    List<dynamic> result = jsonDecode(response.body);
    return result;
  }
}