import 'dart:async';
import 'dart:convert';
import 'package:hackathon_pmp/model/cliente.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

Future<Map<String, dynamic>> registrarCliente (Cliente cliente) async{
  http.Response response = await http.post('https://pmp-hackathon.herokuapp.com/api/cliente/registrar',
    body: jsonEncode(cliente),
    encoding: Encoding.getByName('utf-8'),
    headers: {
      'Content-Type': 'application/json'
    });

  SharedPreferences.getInstance().then((sp) {
    sp.setString("nombres", cliente.nombres);
    sp.setString("apellidos", cliente.apellidos);
    sp.setString("email", cliente.email);
  });

  return jsonDecode(response.body);
}