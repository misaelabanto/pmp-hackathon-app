import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:hackathon_pmp/model/producto.dart';

Future<List<Map<String, dynamic>>> getUltimasCompras ({String idCliente, List<Producto> productos}) async {
  http.Response response = await http.get('https://pmp-hackathon.herokuapp.com/api/venta/listar/$idCliente?quantity=1',
    headers: {
      'Content-Type': 'application/json'
    });

  var json = jsonDecode(response.body);
  print(json);
  return json;
}