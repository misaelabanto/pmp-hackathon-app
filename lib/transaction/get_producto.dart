import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:hackathon_pmp/model/producto.dart';

Future<Producto> getProducto ({String idProducto}) async {
  http.Response response = await http.get('https://pmp-hackathon.herokuapp.com/api/producto/listar/$idProducto',
    headers: {
      'Content-Type': 'application/json'
    });

  var json = jsonDecode(response.body);
  print(json);
  return Producto.fromJson(json);
}