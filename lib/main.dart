import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hackathon_pmp/pages/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PMP Store',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'PMP Store'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('fiis.uni.edu.pe/hackathon_pmp');
  String _battery = 'xd';

  void _openLoginPage() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage())
      );
  }

  Future<void> _showBotonPago() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('showBotonPago');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    setState(() {
      _battery = batteryLevel;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('¿Qué esperas? \nInicia sesión ya', style: Theme.of(context).textTheme.display1),
            SizedBox(height: 30,),
            RaisedButton(
              child: Text(
                'Iniciar Sesión',
                style: Theme.of(context).textTheme.title,
              ),
              textTheme: ButtonTextTheme.primary,
              onPressed: _openLoginPage
            ),
          ],
        ),
      ),
    );
  }
}
