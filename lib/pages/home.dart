import 'package:flutter/material.dart';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:hackathon_pmp/pages/carrito.dart';
import 'package:hackathon_pmp/pages/ultimas_compras.dart';
import 'package:hackathon_pmp/transaction/producto_handler.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {
  List<Producto> productos = [];
  List<String> images = [];
  List<Producto> carrito = [];
  bool isFirst = true;

  @override
  initState() {
    super.initState();
    ProductoHandler.getProductos().then((productosResult) {
      setState(() {
        productos = productosResult;
        print(productos);
      });
    });
  }

  Widget _buildMyListView(_, index) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        child: Row(
          children: productos.length <= 0 ? <Widget>[Container()]: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: _agregarAlCarrito(index),
                    child: Image.network(productos[index%productos.length].imagen,)
                  ),
                  SizedBox(height: 10.0,),
                  Text(productos[index%productos.length].descripcion, softWrap: true,),
                  SizedBox(height: 10.0,),
                  Text('S/${productos[index%productos.length].precio}.00', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),)
                ],
              ),
            ),
            SizedBox(width: 10.0,),
            Expanded(
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: _agregarAlCarrito(index),
                    child: Image.network(productos[(index - 1) %productos.length].imagen),
                  ),
                  SizedBox(height: 10.0,),
                  Text(productos[(index-1)%productos.length].descripcion, softWrap: true,),
                  SizedBox(height: 10.0,),
                  Text('S/${productos[(index-1)%productos.length].precio}.00', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),)
                ],
              ),
            ),
          ],
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PMP Store'),
        elevation: 0,
      ),
      body: SafeArea(
        child: ListView.builder(
          itemBuilder: _buildMyListView,
          itemCount: 10,
        )
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Inicio')),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Carrito'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            title: Text('Últimas compras'),
          ),
        ],
        onTap: (index) {
          if(index == 1) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Carrito(productos: carrito)
              )
            );
          }
          if(index == 2) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => UltimasCompras()
              )
            );
          }
        },
        currentIndex: 0,
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.red,
      ),
    );
  }
  _agregarAlCarrito(index) {
    print('Agregado al carrito');
    carrito.add(productos[index]);
  }
}