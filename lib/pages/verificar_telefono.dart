import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:hackathon_pmp/transaction/verificar_codigo.dart';
import 'package:hackathon_pmp/pages/home.dart';
import 'package:hackathon_pmp/widgets/error_dialog.dart';
export 'package:hackathon_pmp/pages/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerificarTelefonoPage extends StatefulWidget {
  final String telefono;
  final String idCliente;

  VerificarTelefonoPage({Key key, this.telefono, this.idCliente}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VerificarTelefonoState();
}

class _VerificarTelefonoState extends State<VerificarTelefonoPage> {
  final TextEditingController _codigoController = TextEditingController();

  void _verificarCodigo(context) async {
    var tfCodigo = _codigoController.text;
    verificarCodigo(
      idCliente: widget.idCliente,
      codigo: tfCodigo,
      telefono: widget.telefono
    ).then((resultValidacion) {
      if(resultValidacion['status'] == 'verified') {
        SharedPreferences.getInstance().then((sp) {
          sp.setString('idCliente', widget.idCliente);
        });
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage()
          )
        );
      } else {
        showErrorDialog(context, 'El código de verificación no es correcto');
      }
    });
  }

  Widget _buildPageContent(BuildContext context) {
    return Container(
      color: Colors.blue.shade100,
      child: ListView(
        children: <Widget>[
          SizedBox(height: 40.0),
          CircleAvatar(child: Image.asset('assets/img/origami.png'), maxRadius: 50, backgroundColor: Colors.transparent,),
          SizedBox(height: 20.0),
          _buildLoginForm(context),
        ],
      ),
    );
  }

  Container _buildLoginForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: RoundedDiagonalPathClipper(),
            child: Container(
              height: 400,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(40.0)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Hemos enviado un código de verificación a tu celular. Ingrésalo aquí:'),
                  SizedBox(height: 90.0,),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextField(
                      style: TextStyle(color: Colors.blue),
                      controller: _codigoController,
                      decoration: InputDecoration(
                        hintText: "Código de verificación",
                        hintStyle: TextStyle(color: Colors.blue.shade200),
                        border: InputBorder.none,
                        icon: Icon(Icons.lock_outline, color: Colors.blue,)
                      ),
                    )
                  ),
                  SizedBox(height: 10.0,),

                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 40.0,
                backgroundColor: Colors.blue.shade600,
                child: Icon(Icons.phone),
              ),
            ],
          ),
          Container(
            height: 420,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                onPressed: () => _verificarCodigo(context),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                child: Text("Verificar", style: TextStyle(color: Colors.white70)),
                color: Colors.blue,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPageContent(context),
    );
  }
}