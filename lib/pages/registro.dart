import 'package:flutter/material.dart';
import 'package:hackathon_pmp/model/cliente.dart';
import 'package:hackathon_pmp/transaction/registrar_cliente.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:hackathon_pmp/pages/verificar_telefono.dart';
import 'package:hackathon_pmp/transaction/enviar_codigo.dart';
import 'package:hackathon_pmp/widgets/error_dialog.dart';

class RegistroPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => _RestrigoPageState();
}

class _RestrigoPageState extends State<RegistroPage> {

  static final _formKey = GlobalKey<FormState>();

  void _registrarCliente(){
    bool isValid = _formKey.currentState.validate();
    if(!isValid) return;
    var tfNombres = _textControllers['tf_nombres'].text;
    var tfApellidos = _textControllers['tf_apellidos'].text;
    var tfTelefono = _textControllers['tf_telefono'].text;
    var tfPassword = _textControllers['tf_password'].text;
    var tfConfirmPassword = _textControllers['tf_confirm_password'].text;
    var tfEmail = _textControllers['tf_email'].text;
    if(tfPassword != tfConfirmPassword) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Error'),
            content: Text('Las contraseñas no coinciden'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        },
      );
      return;
    }
    var cliente = new Cliente(
      apellidos: tfApellidos,
      email: tfEmail,
      nombres: tfNombres,
      password: tfPassword,
      telefono: tfTelefono
    );
    var idCliente = '';
    registrarCliente(cliente).then((result){
      print(result);
      idCliente = result['idCliente'];
      if(result['error']) {
        showErrorDialog(context, result['message']);
        return;
      }
      enviarCodigo(
        telefono: tfTelefono,
        idCliente: result['idCliente']
      ).then((resultEnvio) {
        if(resultEnvio['status'] == 'success') {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => VerificarTelefonoPage(idCliente: idCliente, telefono: tfTelefono)
            )
          );
        } else {
          showErrorDialog(context, 'Hubo un error al enviar el código de verificación');
        }
      });
    });
  }

  String _passwordValidator(String input) {
    if(input.length < 8){
      return 'La contraseña debe tener más de ocho caracteres';
    }
    var rgxDigitos = new RegExp(r'[0-9]');
    var rgxMayus = new RegExp(r'[A-Z]');
    if(!rgxDigitos.hasMatch(input)) {
      return 'La contraseña debe tener al menos un número';
    }
    if(!rgxMayus.hasMatch(input)){
      return 'La contraseña debe tener al menos una mayúscula';
    }
  }

  String _emailValidator(String input) {
    var rgxEmail = RegExp(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)");
    if(!rgxEmail.hasMatch(input)) {
      return 'El correo electrónico no es válido';
    }
  }

  Map<String, TextEditingController> _textControllers = {
    'tf_nombres': TextEditingController(),
    'tf_apellidos': TextEditingController(),
    'tf_telefono': TextEditingController(),
    'tf_password': TextEditingController(),
    'tf_confirm_password': TextEditingController(),
    'tf_email': TextEditingController()
  };

  @override
  void dispose() {
    _textControllers['tf_nombres'].dispose();
    _textControllers['tf_apellidos'].dispose();
    _textControllers['tf_telefono'].dispose();
    _textControllers['tf_password'].dispose();
    _textControllers['tf_confirm_password'].dispose();
    _textControllers['tf_email'].dispose();
    super.dispose();
  }

  Widget _buildPageContent(BuildContext context) {
    return Container(
      color: Colors.blue.shade100,
      child: ListView(
        children: <Widget>[
          SizedBox(height: 30.0,),
          CircleAvatar(child: Image.asset('assets/img/origami.png'), maxRadius: 50, backgroundColor: Colors.transparent,),
          SizedBox(height: 20.0,),
          _buildLoginForm(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              FloatingActionButton(
                mini: true,
                onPressed: (){
                  Navigator.pop(context);
                },
                backgroundColor: Colors.blue,
                child: Icon(Icons.arrow_back),
              )
            ],
          )
        ],
      ),
    );
  }

  Container _buildLoginForm() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: RoundedDiagonalPathClipper(),
            child: Container(
              height: 600,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(40.0)),
                color: Colors.white,
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 90.0,),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextField(
                        style: TextStyle(color: Colors.blue),
                        controller: _textControllers['tf_nombres'],
                        decoration: InputDecoration(
                          hintText: "Nombres",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.person_outline, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextField(
                        style: TextStyle(color: Colors.blue),
                        controller: _textControllers['tf_apellidos'],
                        decoration: InputDecoration(
                          hintText: "Apellidos",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.person, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.blue),
                        keyboardType: TextInputType.emailAddress,
                        controller: _textControllers['tf_email'],
                        validator: _emailValidator,
                        decoration: InputDecoration(
                          hintText: "Correo electrónico",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.email, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.blue),
                        obscureText: true,
                        controller: _textControllers['tf_password'],
                        validator: _passwordValidator,
                        decoration: InputDecoration(
                          hintText: "Contraseña",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.lock, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextField(
                        style: TextStyle(color: Colors.blue),
                        obscureText: true,
                        controller: _textControllers['tf_confirm_password'],
                        decoration: InputDecoration(
                          hintText: "Confirmar contraseña",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.lock, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: TextField(
                        style: TextStyle(color: Colors.blue),
                        keyboardType: TextInputType.phone,
                        controller: _textControllers['tf_telefono'],
                        decoration: InputDecoration(
                          hintText: "Número de teléfono",
                          hintStyle: TextStyle(color: Colors.blue.shade200),
                          border: InputBorder.none,
                          icon: Icon(Icons.phone, color: Colors.blue,)
                        ),
                      )
                    ),
                    Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),),
                    SizedBox(height: 10.0,),

                  ],
                )
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 40.0,
                backgroundColor: Colors.blue.shade600,
                child: Icon(Icons.person),
              ),
            ],
          ),
          Container(
            height: 620,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                onPressed: _registrarCliente,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                child: Text("Registrarme", style: TextStyle(color: Colors.white70)),
                color: Colors.blue,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPageContent(context),
    );
  }
}