import 'package:flutter/material.dart';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:hackathon_pmp/transaction/open_boton_pago.dart';

class Carrito extends StatefulWidget{

  final List<Producto> productos;

  const Carrito({Key key, this.productos}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CarritoState();

}

class CarritoState extends State<Carrito> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          FlatButton(
            child: Text('PAGAR',
            style: TextStyle(color: Colors.white),),
            onPressed:()async {
              openBotonPago(widget.productos).then((result) {
                showDialog(context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Exito'),
                      content: Text('¡Compra realizada exitosamente'),
                      actions: <Widget>[
                        FlatButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text('OK'),
                        )
                      ],
                    );
                  }
                );
              });
            },
          )
        ],
        title: Text('Carrito'),
      ),
      body: SafeArea(
        child: ListView.builder(
          padding: EdgeInsets.all(16.00),
          itemCount: widget.productos.length,
          itemBuilder: (_, index) {
            return Container(
              child: Column(
                children: <Widget>[
                  Image.network(widget.productos[index].imagen,),
                  SizedBox(height: 10.0,),
                  Text(widget.productos[index].descripcion, softWrap: true,),
                  SizedBox(height: 10.0,),
                  Text('S/${widget.productos[index].precio}.00', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),)
                ],
              ),
            );
          }
        ),
      )
    );
  }

}