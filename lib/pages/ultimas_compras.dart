import 'package:flutter/material.dart';
import 'package:hackathon_pmp/model/producto.dart';
import 'package:hackathon_pmp/transaction/get_producto.dart';
import 'package:hackathon_pmp/transaction/get_ultimas_compras.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UltimasCompras extends StatefulWidget{

  const UltimasCompras({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => UltimasComprasState();

}

class UltimasComprasState extends State<UltimasCompras> {

  List<Producto> _productos = [];

  @override
  void initState() {
    var idCliente = '';
    SharedPreferences.getInstance().then((sp) {
      return sp.getString('idCliente');
    }).then((idCliente) {
      getUltimasCompras(
        idCliente: idCliente
      ).then((compras) {
        for(dynamic xprod in compras[0]['detalle']){
          getProducto(
            idProducto: xprod['idProducto']
          ).then((producto){
            setState(() {
              _productos.add(producto);
            });
          });
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Últimas compras'),
      ),
      body: SafeArea(
        child: ListView.builder(
          padding: EdgeInsets.all(16.00),
          itemCount: _productos.length,
          itemBuilder: (_, index) {
            return Container(
              child: Column(
                children: <Widget>[
                  Image.network(_productos[index].imagen,),
                  SizedBox(height: 10.0,),
                  Text(_productos[index].descripcion, softWrap: true,),
                  SizedBox(height: 10.0,),
                  Text('S/${_productos[index].precio}.00', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),)
                ],
              ),
            );
          }
        ),
      )
    );
  }

}