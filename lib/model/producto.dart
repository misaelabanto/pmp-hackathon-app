class Producto {
  String id;
  String id_categoria;
  String id_marca;
  String descripcion;
  String precio;
  String imagen;
  int matchs;


  Producto(this.id, this.id_categoria, this.id_marca, this.descripcion,
    this.precio, this.imagen, this.matchs);

  Producto.fromJson(Map<String, dynamic> json) :
      this.id = json['id'],
      this.id_categoria = json['id_categoria'],
      this.id_marca = json['id_marca'],
      this.descripcion = json['descripcion'],
      this.precio = json['precio'],
      this.imagen = json['imagen'],
      this.matchs = json['matchs'];
}

