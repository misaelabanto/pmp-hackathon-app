class Cliente {
  Cliente({this.apellidos, this.nombres, this.email, this.telefono, this.password });
  String apellidos;
  String nombres;
  String telefono;
  String email;
  String password;
  Map<String, dynamic> toJson() => {
    'apellidos': this.apellidos,
    'nombres': this.nombres,
    'telefono': this.telefono,
    'email': this.email,
    'password': this.password,
  };

  Cliente.fromJson(Map<String, dynamic> json) :
      this.telefono = json['telefono'],
      this.email = json['email'],
      this.nombres = json['nombres'],
      this.apellidos = json['apellidos'];
}