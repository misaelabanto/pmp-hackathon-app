package pe.edu.uni.fiis.hackathonpmp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.procesos.mc.boton.libreria.CheckSum;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "fiis.uni.edu.pe/hackathon_pmp";

    private String getTransactionSignature() {
        StringBuilder builder = new StringBuilder();
        builder.append("8004749");
        builder.append("O51555456465");
        builder.append("PEN");
        builder.append("1.00");
        builder.append("VErethUtraQuxas57wuMuquprADrAHAb");
        String signature = CheckSum.getSHA1Checksum(builder.toString(), "VErethUtraQuxas57wuMuquprADrAHAb");
        return signature;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);

        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, Result result) {
                        if (methodCall.method.equals("showBotonPago")) {
                            String nombres = methodCall.argument("nombres");
                            String apellidos = methodCall.argument("apellidos");
                            String email = methodCall.argument("email");
                            showBotonPago(nombres, apellidos, email);
                            result.success("Compra con éxito");
                        } else {
                            result.notImplemented();
                        }
                    }
                }
        );
    }

    interface OnTerminatedListener{
        void onTerminated();
    }

    private void showBotonPago(String nombres, String apellidos, String email) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            showBotonPago(nombres, apellidos, email);
        } else {
            Intent it = new Intent(getApplicationContext(), XdActivity.class);
            it.putExtra("nombres", nombres);
            it.putExtra("apellidos", apellidos);
            it.putExtra("email", email);
            startActivityForResult(it, 3579);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }
}
