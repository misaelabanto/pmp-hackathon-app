package pe.edu.uni.fiis.hackathonpmp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.procesos.mc.boton.Mediospago01thirdparties;
import com.procesos.mc.boton.entidades.PaymentInfoE;
import com.procesos.mc.boton.entidades.PaymentResultE;
import com.procesos.mc.boton.entidades.interfaces.PaymentInterface;
import com.procesos.mc.boton.libreria.CheckSum;

public class XdActivity extends Activity implements PaymentInterface {

    private PaymentInfoE opaymentMastercard;
    private static final int ID_FOR_RESULT = 3579;

    private static final String SUB_MERCHANT_CODE = "";
    private static final String API_KEY = "VErethUtraQuxas57wuMuquprADrAHAb";
    private static final String API_LOGIN = "pRRXKOl8ikMmt9u1";
    private static final String MERCHANT_CODE = "8004706";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent it = getIntent();
        Bundle bn = it.getExtras();

        opaymentMastercard = new PaymentInfoE();
        opaymentMastercard.setEvento(PaymentInfoE.AUTORIZACION);
        opaymentMastercard.setRequestCode(252526);
        opaymentMastercard.setApiLogin(API_LOGIN);//  Stu7udrAf5UKamuq  //pRRXKOl8ikMmt9u1
        opaymentMastercard.setApiKey(API_KEY);
        opaymentMastercard.setMerchantcode(MERCHANT_CODE);
        opaymentMastercard.setTransactionCurrency("PEN");
        opaymentMastercard.setTransactionMerchantcode(SUB_MERCHANT_CODE);
        opaymentMastercard.setTransactionOrderNumber("O44005085358");
        opaymentMastercard.setTransactionAmount(1.00);
        opaymentMastercard.setTransactionSignature(getSignature());
        opaymentMastercard.setTransactionLanguage("ES");
        opaymentMastercard.setBuyerTokenId("");
        opaymentMastercard.setBuyerMerchantId("lmarca@mc.com.pe");
        opaymentMastercard.setBuyerDocType("DNI");
        opaymentMastercard.setBuyerDocNumber("");
        opaymentMastercard.setBuyerFirstName(bn.getString("nombres"));
        opaymentMastercard.setBuyerLastName(bn.getString("apellidos"));
        opaymentMastercard.setBuyerEmail(bn.getString("email"));
        opaymentMastercard.setTransactionAutogeneratel("O4400508534987");
        opaymentMastercard.setTransactionAuthDate("20170404");
        opaymentMastercard.setTransactionAuthTime("173526");
        Intent intent = new Intent(XdActivity.this, Mediospago01thirdparties.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(PaymentInfoE.PAYMENT_INFO, opaymentMastercard);
        bundle.putString("entorno", "D");
        intent.putExtras(bundle);
        startActivityForResult(intent, ID_FOR_RESULT);
    }

    public static String getSignature() {

        StringBuilder builder = new StringBuilder();
        builder.append(MERCHANT_CODE);
        builder.append("O44005085358");
        builder.append("PEN");
        builder.append("1.00");
        builder.append(API_KEY);
        String signature = CheckSum.getSHA1Checksum(builder.toString(),API_KEY);
        builder = null;
        return signature;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ID_FOR_RESULT) {
            StringBuilder builder = new StringBuilder();

            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    try {

                        PaymentResultE o = (PaymentResultE) bundle.get(PaymentResultE.PAYMENT_RESULT);
                        builder.append("RequestCode=" + o.getRequestCode() + "\n");
                        builder.append("TransactionCardBrand=" + o.getTransactionCardBrand() + "\n");
                        builder.append("TransactionResult=" + o.getTransactionResult() + "\n");
                        builder.append("MerchantCode=" + o.getMerchantCode() + "\n");
                        builder.append("BuyerTokenId=" + o.getBuyerTokenId() + "\n");
                        builder.append("TransactionOrderNumber=" + o.getTransactionOrderNumber() + "\n");
                        builder.append("TransactionCurrency=" + o.getTransactionCurrency() + "\n");
                        builder.append("TransactionAmount=" + o.getTransactionAmount() + "\n");
                        builder.append("TransactionReferenceNumber=" + o.getTransactionReferenceNumber() + "\n");
                        builder.append("TransactionAuthorizationCode=" + o.getTransactionAuthCode() + "\n");
                        builder.append("TransactionCard=" + o.getTransactionCard() + "\n");
                        builder.append("TransactionCountry=" + o.getTransactionCountry() + "\n");
                        builder.append("TransactionCuotasNumber=" + o.getTransactionCuotasNumber() + "\n");
                        builder.append("TransactionFirstCuotaDate=" + o.getTransactionFirstCuotaDate() + "\n");
                        builder.append("TransactionCuotaCurrency=" + o.getTransactionCuotaCurrency() + "\n");
                        builder.append("TransactionCuotaAmount=" + o.getTransactionCuotaAmount() + "\n");
                        builder.append("TransactionId=" + o.getTransactionId() + "\n");
                        builder.append("TransactionAuthorizationDate=" + o.getTransactionAuthDate() + "\n");
                        builder.append("TransactionAuthorizationTime=" + o.getTransactionAuthTime() + "\n");
                        builder.append("NumberAttemptDeniedDay=" + o.getNumberAttemptDeniedDay() + "\n");
                        builder.append("NumberAttemptApprovedDay=" + o.getNumberAttemptApprovedDay() + "\n");
                        builder.append("ResponseCode=" + o.getResponseCode() + "\n");
                        builder.append("ResponseMessage=" + o.getResponseMessage() + "\n");

                        Log.d("MASTERCARD :", builder.toString());

                    } catch (Exception e) {
                        builder.append("Exception: " + e.getMessage());
                    }
                } else builder.append("Intent tiene Bundle NULL");
            } else builder.append("Intent es NULL");
        }
    }


    @Override
    public void onPaymentHandle(String s) {
        Log.d("onPaymentHandle :", s);
    }
}
